<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 04.12.2017
 * Time: 23:22
 */

setlocale(LC_ALL, 'ru_RU.UTF-8');
header('Content-Type: text/html; charset=utf-8', true);
require_once('lib/Parser.php');


ob_implicit_flush(1);

echo str_pad('', 1024);
@ob_flush();
flush();
$parser=new Parser();
$products=$parser->getProductsFromDb();//getListProducts();
/*echo "<pre>r";
print_r($products);
echo "</pre>";*/
$products_csv='';
$headers_array=['Артикул','Название','Штрихкод','Цена','Бренд','Фасовка'];
$header='Артикул;Название;Штрихкод;Цена;Бренд;Фасовка;';
$headers_array_attr=[];
$delimeter=";";
foreach ($products as $product)
{
    $attributes=$product['attributes'];
    foreach ($attributes as $attr){
        $attr_explode=explode('=',$attr);
        $name=$attr_explode[0];
        $value=$attr_explode[1];
        $headers_array_attr[$name]=$name;
    }
}

$products_csv.=$header.implode(';',$headers_array_attr)."\r\n";
foreach ($products as $product)
{
    $attributes=$product['attributes'];
    $new_data=[
        'sku'=>$product['sku'],
        'name'=>$product['name'],
        'barcode'=>$product['barcode'],
        'price'=>$product['price'],
        'brand'=>$product['brand'],
        'packaging'=>$product['packaging']
    ];
    $temp_attr=[];
    foreach ($attributes as $attr) {
        $attr_explode = explode('=', $attr);

        $name = $attr_explode[0];
        $value = $attr_explode[1];
        $temp_attr[$name]=$value;

    }
    //$products_csv.=$product['sku'].$delimeter.$product['name'].$delimeter.$product['barcode'].$delimeter.$product['price'].$delimeter.$product['brand'].$delimeter.$product['packaging'].$delimeter;
    foreach ($headers_array_attr as $at) {

        if (isset($temp_attr[$at]))
        {
            $new_data[]=$temp_attr[$at];
        }else{
            $new_data[]='';
        }
    }
    $products_csv.=implode($delimeter,$new_data).";\r\n";

    //$data



}


    $file='allproducts.csv';
	$handle = fopen($file, "w+");

		fwrite($handle,header('Content-Type: text/html; charset=utf8'));
			fwrite($handle, $products_csv);
			fclose($handle);

