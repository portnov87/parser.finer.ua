<?php
setlocale(LC_ALL, 'ru_RU.UTF-8');
header('Content-Type: text/html; charset=utf-8', true);
//error_reporting(0);
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 03.12.2017
 * Time: 16:16
 */
require_once('safemysql.class.php');

class Parser
{
    public $base_url = 'http://fozzyshop.com.ua/';
    public $base_url_slesh = 'http://fozzyshop.com.ua';
    public $db = false;
    public $admin_emails = ['a@finer.ua', 's@finer.ua', 'portnovvit@gmail.com'];
    public $url_parser = 'http://parser.finer.ua';


    public function __construct()
    {
        $opts = array(
            'user' => 'root',
            'pass' => '',
            'db' => 'parserurl',
            'charset' => 'utf-8'
        );
        $db = new SafeMySQL();//$opts);
        $this->db = $db;
    }

    /**
     * @param $data
     * Добавляем разультат выдачи парсера в бд
     */
    public function insertdb($type, $dataIn, $url = '', $flag_update = false)
    {
        $date = date('Y-m-d m:s:D');
        switch ($type) {
            case 'category':
                $data = array(
                    'http_code' => $dataIn['http_code'],
                    'errors' => $dataIn['errors'],
                    //'html' => $dataIn['html']
                );
                if (isset($dataIn['parent'])) $data['parent'] = $dataIn['parent'];
                if (isset($dataIn['parent_id'])) $data['parent_id'] = $dataIn['parent_id'];
                if (isset($dataIn['name'])) $data['name'] = $dataIn['name'];
                if (isset($dataIn['url'])) {
                    $data['url'] = $dataIn['url'];
                    $url = $dataIn['url'];
                } else $data['url'] = $url;

                $exist_data = $this->db->getAll("SELECT `http_code`,`id`,`name`,`shop_id`,`name`,`parent`,`parent_id`,`url`,`html`,`errors` FROM ?n WHERE url = ?s LIMIT 1", 'category', $url);

                if (count($exist_data) == 0) {
                    $sql = "INSERT INTO category SET date_update=NOW(),date_add= NOW(),?u";
                    //" ON DUPLICATE KEY UPDATE date_add= NOW(),?u";
                    $this->db->query($sql, $data);
                    return ['id' => $this->db->insertId(), 'url' => $url, 'errors' => '', 'http_code' => $dataIn['http_code'], 'name' => $dataIn['name'],
                        'html' => $dataIn['html']
                    ];
                } else {
                    if ($flag_update || $exist_data[0]['html'] == '') {
                        $sql = "UPDATE category SET date_update=NOW(),?u WHERE url=?s";//" ON DUPLICATE KEY UPDATE date_add= NOW(),?u";
                        $this->db->query($sql, $data, $url);
                        //return $exist_data[0]['id'];
                    }
                    $res = ['id' => $exist_data[0]['id'], 'errors' => '', 'url' => $url, 'http_code' => $dataIn['http_code'],
                        'html' => $dataIn['html']
                    ];
                    return $res;
                }
                break;

        }
        return false;
    }

    public function updateParentName($parent_url, $name)
    {
        $data = ['name' => $name];
        $sql = "UPDATE category SET ?u WHERE url=?s";//" ON DUPLICATE KEY UPDATE date_add= NOW(),?u";
        $this->db->query($sql, $data, $parent_url);
        return $data;
    }

    /**
     * @param $url
     * @param bool $flag_update
     * @return array|bool
     */
    public function SelectResultFromDb($url, $flag_update = false)
    {
        //if ($flag_update) {
        $exist_data = $this->db->getAll("SELECT * FROM ?n WHERE url = ?s", 'category', $url);
        if (count($exist_data) > 0) {
            $data = array('code' => $exist_data[0]['http_code'], 'errors' => $exist_data[0]['errors'], 'html' => $exist_data[0]['html']);
            return $data;

        }
        //}
        return false;

    }

    /**
     * @return array
     */
    public function getListCategories($category_id = 0)
    {
        if ($category_id == 0)
            $categories_data = $this->db->getAll("SELECT id,url,name,shop_id,parent,parent_id FROM ?n", 'category');
        else
            $categories_data = $this->db->getAll("SELECT id,url,name,shop_id,parent,parent_id  FROM ?n WHERE id=?i", 'category', $category_id);
        return $categories_data;
    }

    /**
     * @return array
     */
    public function getShops()
    {
        $shops_data = $this->db->getAll("SELECT * FROM ?n", 'shops');
        $result_data = [];
        foreach ($shops_data as $data) {
            $result_data[$data['id']] = $data['shopname'];
        }
        return $result_data;
    }

    /**
     * @param $url
     * @param $shop
     * @return array|bool|FALSE|resource
     */
    public function newUrl($url, $shop)
    {

        $exist_data = $this->db->getAll("SELECT id,parent, parent_id, name, status_parser, queue,url FROM ?n WHERE url = ?s", 'category', $url);

        if (count($exist_data) > 0) {
            foreach ($exist_data as $data) {
                return $data;
            }
        } else {
            $sql = "INSERT INTO category SET date_add=NOW(),?u";
            $data = [];
            $data['url'] = $url;
            $data['shop'] = $shop;
            $data['status_parser'] = 0;
            $data['queue'] = 1;
            return $this->db->query($sql, $data);
        }

        return false;
    }

    /**
     * @param $parent
     * @param array $cats
     * @return array
     */
    public function getCategoriesByParent($parent, $cats = [])
    {
        $categories = $this->db->getAll("SELECT `name`,`id`,`parent`,`parent_id`,`url` FROM ?n WHERE parent_id=?i", 'category', $parent);

        if (count($categories) > 0) {
            $cats = array_merge($cats, $categories);
            foreach ($categories as $cat) {
                $cats = $this->getCategoriesByParent($cat['id'], $cats);
            }
        }

        return $cats;
    }

    /**
     * @param int $parent
     * @return array
     */
    public function getCategories($parent = 0)
    {
        $levels = array();
        $tree = array();
        $cur = array();

        if ($parent == 0)
            $categories = $this->db->getAll("SELECT `name`,`id`,`parent`,`parent_id`,`url` FROM ?n", 'category');
        else
            $categories = $this->getCategoriesByParent($parent);

        foreach ($categories as $row) {
            $cur = &$levels[$row['id']];
            $cur['parent_id'] = $row['parent_id'];
            $cur['name'] = $row['name'];

            if ($row['parent_id'] == $parent)
                $tree[$row['id']] = &$cur;
            else
                $levels[$row['parent_id']]['children'][$row['id']] = &$cur;
        }
        return $tree;
    }

    /**
     * @return array
     */
    public function getTreeCategories($arr = [], $level = 0)
    {
        $out = '';
        $cats = [];
        $str_name = '';
        for ($i = 1; $i <= $level; $i++)
            $str_name .= '-';

        foreach ($arr as $k => $v) {
            $_level = $level;
            $out .= '<tr><td>' . $str_name . $v['name'] . '</td><td>' . $v['url'] . '</td></tr>';
            if (isset($v['children'])) {
                if (is_array($v['children'])) {
                    if (count($v['children']) > 0) {
                        $_level++;
                        $out .= $this->getTreeCategories($v['children'], $_level);
                    }
                }
            }
        }
        return $out;
    }

    /**
     *
     */
    public function parser_categories()
    {
        $categories_data = $this->getListCategories();
        foreach ($categories_data as $category) {
            $url = $category['url'];
            echo $url . "<br/>\r\n";
            $this->get_content_category($url);
        }
        return true;
    }

    /**
     *
     */
    public function parserCategoriesWithProducts($category_id, $only_price = false, $queue_id = false)
    {
        $categories_data = $this->getListCategories($category_id);
        $i = 0;
        $errors = [];
        $result_status = true;

        foreach ($categories_data as $category) {
            /*echo "<pre>cat";
            print_r($category);
            echo "</pre>";*/
            $url = $category['url'];
            if ($res = $this->get_content_category($url, true, $only_price, $queue_id)) {
                if ($res['error'] != '') {
                    $errors[] = $res['error'];
                    $result_status = false;
                }
            } else $result_status = false;
            $i++;
        }


        return ['result' => $result_status, 'errors' => implode("<br/>", $errors)];
    }

    public function getCategoryByName($name)
    {
        $name = str_replace('&nbsp;', '', $name);
        $name = str_replace('&quot;', '', $name);

        $exist_data = $this->db->getAll("SELECT * FROM ?n WHERE name = ?s", 'category', $name);
        if (count($exist_data) > 0) {
            return $exist_data[0];
        }
        return false;
    }

    /**
     * @param $url
     * @return mixed
     */
    public function get_content_category($url, $parse_products = false, $only_price = false, $queue_id = false)
    {
        sleep(1);
        $flag_update = true;//false;
        $base_url = $this->base_url;
        $response_arr = $this->SelectResultFromDb($url, $flag_update);
        // если flag_update true то берём с бд, если flag_update true то парсим данные по урлу
        if (($response_arr && $flag_update)
            || (!$response_arr)
            || ($response_arr['html'] == '')
        ) {
            $_response_arr = $this->curl_get_contents($url, $base_url, 5, 1);
            $response_arr = $this->insertdb('category', $_response_arr, $url, $flag_update);
        }

        $page = $response_arr['html'];
        $page_code = $response_arr['http_code'];
        $res_arr['error_page'] = $response_arr['errors'];
        /*
        echo "<pre>res";
        print_r($response_arr);
        echo "</pre>";*/
        if (!empty($page) && $page_code == 200) {


            $buffer = array();

            $regexp = "/<h1 (.*)>(.*)<\/h1>/Us";
            preg_match($regexp, $page, $buffer);
            $name = '';
            if (isset($buffer[2])) {
                $name = strip_tags($buffer[2]);
                $name = str_replace('&nbsp;', '', $name);
                $name = str_replace('&quot;', '', $name);

                $this->updateParentName($url, $name);
            }

            // ищем субкатегории
            $buffer = [];
            $regexp = "/<div id=\"subcategories\">(.*)<\/div>/Us";
            $regexp = "/<a class=\"subcategory-name\" href=\"(.*)\">(.*)<\/a>/Us";

            preg_match_all($regexp, $page, $buffer);

            $sub_categories = [];

            if (isset($buffer[1])) {
                foreach ($buffer[1] as $key => $_sub_category) {
                    if (isset($buffer[2])) {
                        $name_sub = str_replace('&nbsp;', '', $buffer[2][$key]);
                        $name_sub = str_replace('&quot;', '', $name_sub);
                        $parent = $this->getCategoryByName($name);
                        if ($parent)
                            $parent_id = $parent['id'];
                        else $parent_id = false;

                        $sub_category_insert = [
                            'parent_id' => $parent_id, 'parent' => $name, 'http_code' => '', 'errors' => '', 'url' => $_sub_category, 'html' => '', 'name' => $name_sub];//
                        $cat_id = $this->insertdb('category', $sub_category_insert, '', true);


                        $sub_categories[] = $sub_category_insert;
                    }
                }
            }
            if (count($sub_categories) > 0) {
                $this->updateissetProduct($url,0);
                foreach ($sub_categories as $sub) {
                    $sub_url = $sub['url'];
                    return $this->get_content_category($sub_url, $parse_products, $only_price, $queue_id);
                }
            } else {
                $this->updateissetProduct($url,1);
                if ($parse_products) {
                    $this->get_list_products($response_arr, true, $only_price, $queue_id);
                }
            }


            $res_arr['name'] = $name;
            $res_arr['links'] = '';
            $res_arr['content'] = '';
            $res_arr['error'] = '';
        } else {
            $res_arr['price'] = 0;
            $res_arr['currency'] = 'nodata';
            $res_arr['error'] = 'Ошибка загрузки страницы: ' . $url;
        }
        return $res_arr;
    }


    /* --- 1.2 --- Загрузка страницы при помощи cURL */
    public function curl_get_contents($page_url, $base_url, $pause_time, $retry)
    {

        /*
        $page_url - адрес страницы-источника
        $base_url - адрес страницы для поля REFERER
        $pause_time - пауза между попытками парсинга
        $retry - 0 - не повторять запрос, 1 - повторить запрос при неудаче
        */
        $error_page = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0");
        //curl_setopt($ch, CURLOPT_COOKIEJAR, str_replace("\\", "/", getcwd()).'/gearbest.txt');
        //curl_setopt($ch, CURLOPT_COOKIEFILE, str_replace("\\", "/", getcwd()).'/gearbest.txt');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // Автоматом идём по редиректам
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // Не проверять SSL сертификат
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // Не проверять Host SSL сертификата
        curl_setopt($ch, CURLOPT_URL, $page_url); // Куда отправляем
        curl_setopt($ch, CURLOPT_REFERER, $base_url); // Откуда пришли
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Возвращаем, но не выводим на экран результат
        $response['html'] = curl_exec($ch);
        //$response['html']=mb_detect_encoding($response['html'], array('UTF-8', 'Windows-1251')).$response['html'];
        $info = curl_getinfo($ch);
        if ($info['http_code'] != 200 && $info['http_code'] != 404) {
            $error_page[] = array(1, $page_url, $info['http_code']);
            if ($retry) {
                sleep($pause_time);
                $response['html'] = curl_exec($ch);
                $info = curl_getinfo($ch);
                if ($info['http_code'] != 200 && $info['http_code'] != 404)
                    $error_page[] = array(2, $page_url, $info['http_code']);
            }
        }
        $response['http_code'] = $info['http_code'];
        $response['errors'] = $error_page;
        curl_close($ch);
        return $response;
    }


    /* --- 1.4.2 --- Вывод ошибок */
    public function error_list_html($error)
    {
        if (!empty($error)) {
            echo "<p>Во время обработки запроса произошли следующие ошибки:</p>\n";
            echo "<ul>\n";
            foreach ($error as $error_row) {
                echo "<li>" . $error_row . "</li>\n";
            }
            echo "</ul>\n";
            echo "<p>Статус: <span class=\"red\">FAIL</span></p>\n";
        } else {
            echo "<p>Статус: <span class=\"green\">OK</span></p>\n";
        }
    }

    /* --- 1.4.3 --- Вывод ошибок загрузки страниц */
    public function error_page_list_html($error_page)
    {
        if (!empty($error_page)) {
            echo "<ul>\n";
            foreach ($error_page as $error_row) {
                echo "<li>[" . $error_row[0] . "] " . $error_row[1] . " - " . $error_row[2] . "</li>\n";
            }
            echo "</ul>\n";
        }
    }

    /* --- 1.4.4 --- Вывод работы скрипта */
    public function run_time_html($time_start)
    {
        if (!empty($time_start))
            echo "<!--p>Время работы: " . (time() - $time_start) . "</p-->\n";
    }


    /**
     * @param $url_category
     */
    public function getProducts($url_category, $update = true)
    {

        $category_data = $this->db->getAll("SELECT * FROM ?n WHERE url=?s", 'category', $url_category);
        $i = 0;
        $result = [];
        if (isset($category_data[0])) {
            $category = $category_data[0];
            $products_list = $this->get_list_products($category, $update);
            if (count($products_list) > 0) {
                return $products_list;

            }

        }

        return false;

    }

    /**
     * @param $page
     * @return bool|mixed
     */
    public function getProductsFromHtml($page)
    {
        $buffer = [];
        $regexp = "/<ul id=\"product_list\" class=\"product_list grid row\">(.*)<\/ul>/Us";
        preg_match($regexp, $page, $buffer);

        if (isset($buffer[1])) {
            $page_list_products = $buffer[1];
            $regexp = "/<a class=\"product-name\" href=\"(.*)\" title=\"(.*)\">(.*)<\/a>/Us";
            $buffer = [];
            preg_match_all($regexp, $page_list_products, $buffer);
            if (isset($buffer[1])) {
                return $buffer[1];
            }
        }

        return false;
    }

    /**
     * @param $page
     * @param $href_products
     * @return array
     */
    public function getProductFromPagination($page, $href_products)
    {
        $buffer = [];
        $regexp = "/<li id=\"pagination_next\" class=\"pagination_next\">(.*)<\/li>/Us";

        preg_match($regexp, $page, $buffer);
        if (isset($buffer[1])) {
            $page_list_products = $buffer[1];
            $regexp = "/<a href=\"(.*)\"\s*>(.*)<\/a>/Us";
            $buffer = [];
            preg_match($regexp, $page_list_products, $buffer);
            if (isset($buffer[1])) {
                $url = str_replace('rel="next', '', $buffer[1]);
                $url = trim($this->base_url_slesh . str_replace('"', '', $url));

                $_response_arr = $this->curl_get_contents($url, $this->base_url, 5, 1);
                $page = $_response_arr['html'];
                $page_code = $_response_arr['http_code'];
                if (!empty($page) && $page_code == 200) {
                    $href_products_temp = $this->getProductsFromHtml($page);
                    $href_products = array_merge($href_products, $href_products_temp);
                    // ищем рекурсивно по ссылке Следующая страница
                    return $this->getProductFromPagination($page, $href_products);
                }
            }

        }
        return $href_products;
    }

    /**
     * @param $category_html
     */
    public function get_list_products($response_arr, $update = true, $only_price = false, $queue_id = false)
    {

        $category_id = $response_arr['id'];
        $page = $response_arr['html'];
        $page_code = $response_arr['http_code'];
        $res_arr['error_page'] = $response_arr['errors'];
        $href_products = [];
        if (!empty($page) && $page_code == 200) {
            $href_products = [];
            $href_products = $this->getProductsFromHtml($page);
            // теперь необходимо пройтись по пагинации
            $href_products = $this->getProductFromPagination($page, $href_products);
            if ($href_products) {

                if (count($href_products) > 0) {
                    // парсим товары по ссылкам
                    $products = $this->get_content_products($href_products, $category_id, $update, $only_price, $queue_id);
                }

            } else return [];
        }
        return $href_products;

    }


    public function getFieldsProduct($page, $url, $errors, $page_code, $category_id)
    {
        $name = $barcode = $sku = $brand = $photo = $packaging = '';
        $price = 0;
        $attributes = [];
        $images = [];
        $regexp = "/<h1 (.*)>(.*)<\/h1>/Us";
        $buffer = [];
        preg_match($regexp, $page, $buffer);

        // name
        if (isset($buffer[2]))
            $name = strip_tags($buffer[2]);
        // price
        $buffer = [];
        $regexp = "/<span id=\"our_price_display\" itemprop=\"price\">(.*)<\/span>/Us";
        preg_match($regexp, $page, $buffer);
        if (isset($buffer[1])) {
            $price = strip_tags($buffer[1]);
            $price = trim(str_replace('грн.', '', $price));
            $price_ex = explode(' ', $price);
            //if (isset($price_ex[0]))
            $price = trim(str_replace(',', '.', $price_ex[0]));
            $price = (float)$price;
            //echo '$price'.$price;die();
        }

        $buffer = [];
        $regexp = "/<div class=\"product-title\">(.*)<\/div>/Us";
        preg_match($regexp, $page, $buffer);
        if (isset($buffer[1])) {
            $html_addition = $buffer[1];
            //brand
            $buffer = [];
            $regexp = "/<a itemprop=\"brand\" (.*)>(.*)<\/a>/Us";
            preg_match($regexp, $html_addition, $buffer);
            if (isset($buffer[2])) {
                $brand = $buffer[2];
                $brand = trim(strip_tags($brand));
            }

            $buffer = [];
            // фасовка
            $regexp = "/<p id=\"product_unity\">(.*)<\/p>/Us";
            preg_match($regexp, $html_addition, $buffer);
            if (isset($buffer[1])) {
                $html = $buffer[1];
                $buffer = [];
                $regexp = "/<span (.*)>(.*)<\/span>/Us";
                preg_match($regexp, $html, $buffer);
                $packaging = $buffer[2];
                $packaging = trim(strip_tags($packaging));
            }

            $buffer = [];
            // артикул
            $regexp = "/<p id=\"product_reference\">(.*)<\/p>/Us";
            preg_match($regexp, $html_addition, $buffer);
            if (isset($buffer[1])) {
                $html = $buffer[1];
                $buffer = [];
                $regexp = "/<span (.*)>(.*)<\/span>/Us";
                preg_match($regexp, $html, $buffer);
                $sku = $buffer[2];

                $sku = trim(strip_tags($sku));
            }

            $buffer = [];
            // штрихкод
            $regexp = "/<p id=\"product_ean13\">(.*)<\/p>/Us";
            preg_match($regexp, $html_addition, $buffer);
            if (isset($buffer[1])) {
                $html = $buffer[1];
                $buffer = [];
                $regexp = "/<span (.*)>(.*)<\/span>/Us";
                preg_match($regexp, $html, $buffer);
                $barcode = $buffer[2];
                $barcode = trim(strip_tags($barcode));
            }
        }

        $images = $this->parsePhoto($page);

        $attributes = [];
        $buffer = [];
        // характеристики
        $regexp = "/<section (.*) id=\"featuresTab\">(.*)<\/section>/Us";
        preg_match($regexp, $page, $buffer);

        if (isset($buffer[2])) {
            $doc = new DOMDocument('1.0', 'utf-8');
            $content = $buffer[2];//
            $content = mb_convert_encoding($buffer[2], 'HTML-ENTITIES', 'UTF-8');
            $doc->loadHTML($content);
            $searchNodes = $doc->getElementsByTagName("tr");
            foreach ($searchNodes as $node) {
                $attr_node = $node->getElementsByTagName("td");
                $attr_name = $attr_node[0]->nodeValue;
                $attr_value = $attr_node[1]->nodeValue;
                $attributes[] = $attr_name . '=' . $attr_value;
            }
        }


        return [
            'category_id' => $category_id,
            'html' => $page,
            'errors' => $errors,
            'http_code' => $page_code,
            'url' => $url,
            'name' => $name,
            'barcode' => $barcode,
            'sku' => $sku,
            'price' => $price,
            'brand' => $brand,
            'packaging' => $packaging,
            'attributes' => $attributes,
            'images' => $images
        ];
    }

    /**
     * @param $urls - массив ссылок продуктов
     * парсим продукты с источника
     */
    public function get_content_products($urls, $category_id, $update = true, $only_price = false, $queue_id = false)
    {
        //Артикул, Название, Штрихкод, Цена, Бренд, Фасовка
        $data = [];
        foreach ($urls as $url) {
            $exist_product = false;
            // если стоит флаг update = false значит товары которые есть в базе не обновлять
            if (!$update) {
                $exist_data = $this->db->getAll("SELECT * FROM ?n WHERE url = ?s LIMIT 1", 'products', $url);
                if (count($exist_data) > 0) continue;
            } else {
                $exist_data = $this->db->getAll("SELECT * FROM ?n WHERE url = ?s LIMIT 1", 'products', $url);
                if (count($exist_data) > 0)
                    $exist_product = $exist_data[0];
                else $exist_product = false;
            }

            $response_arr = $this->curl_get_contents($url, $this->base_url, 5, 1);
            $page = $response_arr['html'];
            $page_code = $response_arr['http_code'];
            $errors = $response_arr['errors'];

            $name = $barcode = $sku = $brand = $photo = $packaging = '';
            $price = 0;
            $attributes = [];
            if (!empty($page) && $page_code == 200) {


                if ((!$exist_product && $only_price) || (!$only_price)) {
                    $fields = $this->getFieldsProduct($page, $url, $errors, $page_code, $category_id);


                    $data = [
                        'category_id' => $category_id,
                        'html' => $page,
                        'errors' => $errors,
                        'http_code' => $page_code,
                        'url' => $url,
                        'name' => $fields['name'],
                        'barcode' => $fields['barcode'],
                        'sku' => $fields['sku'],
                        'price' => $fields['price'],
                        'brand' => $fields['brand'],
                        'packaging' => $fields['packaging'],
                        'attributes' => $fields['attributes'],
                        'images' => $fields['images']
                    ];
                } elseif ($only_price) {


                    $regexp = "/<h1 (.*)>(.*)<\/h1>/Us";
                    $buffer = [];
                    preg_match($regexp, $page, $buffer);

                    $price = 0;
                    // name
                    if (isset($buffer[2]))
                        $name = strip_tags($buffer[2]);
                    // price
                    $buffer = [];
                    $regexp = "/<span id=\"our_price_display\" itemprop=\"price\">(.*)<\/span>/Us";
                    preg_match($regexp, $page, $buffer);
                    if (isset($buffer[1])) {
                        $price = strip_tags($buffer[1]);
                        $price = trim(str_replace('грн.', '', $price));
                        $price_ex = explode(' ', $price);
                        //if (isset($price_ex[0]))
                        $price = trim(str_replace(',', '.', $price_ex[0]));
                        $price = (float)$price;
                    }
                    $exist_data = $this->db->getAll("SELECT * FROM ?n WHERE url = ?s LIMIT 1", 'products', $url);
                    $product_id = false;
                    if (count($exist_data) == 0) {

                        $fields = $this->getFieldsProduct($page, $url, $errors, $page_code, $category_id);

                        $data = [
                            'category_id' => $category_id,
                            'html' => $page,
                            'errors' => $errors,
                            'http_code' => $page_code,
                            'url' => $url,
                            'name' => $fields['name'],
                            'barcode' => $fields['barcode'],
                            'sku' => $fields['sku'],
                            'price' => $fields['price'],
                            'brand' => $fields['brand'],
                            'packaging' => $fields['packaging'],
                            'attributes' => $fields['attributes'],
                            'images' => $fields['images']
                        ];
                    } else {
                        $data = [
                            'url' => $url,
                            'price' => $price,
                            'errors' => '',
                        ];
                    }
                }


                $this->saveToDbProduct($data, $only_price, $queue_id);
                sleep(1);

            }
        }


    }

    public function saveToDbProduct($new_product, $only_price = false, $queue_id = false)
    {
        //  foreach ($data as $new_product)
        // {
        /*  echo "<pre>";
          print_r($new_product);
          echo "</pre>";*/

        $url = $new_product['url'];
        $exist_data = $this->db->getAll("SELECT * FROM ?n WHERE url = ?s LIMIT 1", 'products', $url);
        $product_id = false;
        $save = false;
        if (count($exist_data) == 0) {
            $sql = "INSERT INTO products SET date_update=NOW(),date_add=NOW(),?u";
            /*if ($only_price){
                $data_insert=[
                    'price' => $new_product['price'],
                ];

            }else {*/

            $data_insert = [
                'name' => $new_product['name'],
                'sku' => $new_product['sku'],
                'barcode' => $new_product['barcode'],
                'price' => $new_product['price'],
                'brand' => $new_product['brand'],
                'packaging' => $new_product['packaging'],
                'url' => $new_product['url'],
                'category_id' => $new_product['category_id'],
                //'html' => $new_product['html'],
                'http_code' => $new_product['http_code'],
                'attributes' => serialize($new_product['attributes']),
                'errors' => $new_product['errors']
            ];
            //}
            if ($this->db->query($sql, $data_insert))
                $save = true;
            $product_id = $this->db->insertId();

            /*  echo '<pre>insert';
              print_r($data_insert);
              echo '</pre>';
              die();*/
        } else {

            $product_id = $exist_data[0]['id'];
            $sql = "UPDATE products SET date_update=NOW(),?u WHERE url=?s";
            if ($only_price) {
                //$sql = "UPDATE products SET date_update=NOW(),?u WHERE url=?s";
                $data_insert = [
                    'price' => $new_product['price'],
                ];
                if ($new_product['name'])
                    $data_insert['name'] = $new_product['name'];
                if ($new_product['sku'])
                    $data_insert['sku'] = $new_product['sku'];
                if ($new_product['barcode'])
                    $data_insert['barcode'] = $new_product['barcode'];
                if ($new_product['brand'])
                    $data_insert['brand'] = $new_product['brand'];
                if ($new_product['packaging'])
                    $data_insert['packaging'] = $new_product['packaging'];
                if ($new_product['category_id'])
                    $data_insert['categoy_id'] = $new_product['categoy_id'];
                if ($new_product['attributes'])
                    $data_insert['attributes'] = serialize($new_product['attributes']);


            } else {

                $data_insert = [
                    'name' => $new_product['name'],
                    'sku' => $new_product['sku'],
                    'barcode' => $new_product['barcode'],
                    'price' => $new_product['price'],
                    'brand' => $new_product['brand'],
                    'packaging' => $new_product['packaging'],
                    'category_id' => $new_product['category_id'],
                    // 'html' => $new_product['html'],
                    'http_code' => $new_product['http_code'],
                    'attributes' => serialize($new_product['attributes']),
                    'errors' => $new_product['errors']
                ];
            }

            /*echo '<pre>';
            print_r($data_insert);
            echo '</pre>';
            die();*/
            if ($this->db->query($sql, $data_insert, $url)) $save = true;
        }
        if ($product_id) {
            if (isset($new_product['images'])) {
                if (count($new_product['images']) > 0) {
                    $this->db->query("DELETE FROM product_images WHERE product_id=?i", $product_id);
                    foreach ($new_product['images'] as $image) {
                        $sql = "INSERT INTO product_images SET date_update=NOW(),date_add=NOW(),?u";
                        $this->db->query($sql, [
                            'product_id' => $product_id,
                            'path' => $image,
                        ]);
                    }
                }
            }
        }

        if ($save && $queue_id) {
            $sql = "INSERT INTO queue_logs SET queue_id=?s,product_id=?s,status=?i";
            $this->db->query($sql, $queue_id, $product_id, 1);

        }

        //}
    }

    /**
     * @param $page
     * @return array
     */
    public function parsePhoto($page)
    {
        $result = [];
        $buffer = [];
        $regexp = "/<div (.*) class=\"MagicToolboxSelectorsContainer\">(.*)<\/div>/Us";
        preg_match($regexp, $page, $buffer);
        /* echo "<pre>images";
         print_r($buffer);
         echo "</pre>";die();*/
        $buffer_img = '';
        if (isset($buffer[2])) {
            $buffer_img = $buffer[2];
        } else {
            $buffer = [];
            $regexp = "/<div (.*) class=\"MagicToolboxSelectorsContainer MagicScroll\" (.*)>(.*)<\/div>/Us";
            preg_match($regexp, $page, $buffer);
            if (isset($buffer[3])) {
                $buffer_img = $buffer[3];
            }
        }
        if ($buffer_img != '') {
            $doc = new DOMDocument();
            $doc->loadHTML($buffer_img);

            $searchNodes = $doc->getElementsByTagName("a");
            foreach ($searchNodes as $node) {
                $result[] = $node->getAttribute('href');
            }


        }
        return $result;
    }

    /**
     * @param $cats
     * @param array $products
     * @return array
     */
    public function getProductsRecursive($cats, $products = [], $category_id = false)
    {
        foreach ($cats as $id => $cat) {

            if (isset($cat['children'])) {
                $products = $this->getProductsRecursive($cat['children'], $products, $category_id);

            } else {
                $prods = $this->getProductsFromDbByCategory($id);
                $products = array_merge($products, $prods);
            }
        }
        if ($category_id) {
            $prods = $this->getProductsFromDbByCategory($category_id);
            $products = array_merge($products, $prods);
        }

        $result = [];
        foreach ($products as $product) {
            $attributes = [];
            if ($product['attributes'] != '') {

                $attributes = @unserialize($product['attributes']);
            }

            $data = $product;
            $data['attributes'] = $attributes;
            $result[] = $data;
        }
        return $result;

        //return $products;

    }

    /**
     * @param $category_id
     * @return array
     */
    public function getSubCategoriesFromDB($category_id=false)
    {
        $result_data = [];
        if ($category_id)
            $cats = $this->getCategories($category_id);
        else $cats = $this->getCategories();
        $result_data['categories'] = $cats;
        $products = $this->getProductsRecursive($cats, [], $category_id);
        $result_data['products'] = $products;
        return $result_data;
    }


    /**
     * @param $category_id
     * @return array
     */
    public function getProductsFromDbByCategory($category_id)
    {
        $result = [];
        $products_data = $this->db->getAll("SELECT products.attributes,products.name,products.barcode,products.brand,products.packaging,products.url,products.sku,products.price,products.date_update, category.name as category_name FROM ?n
          LEFT JOIN category ON (products.category_id=category.id) WHERE products.category_id=?i", 'products', $category_id);
        return $products_data;
    }

    /**
     * @return array
     */
    public function getProductsFromDb($category_id = false)
    {
        $result = [];

        $query = "SELECT * FROM ?n";
        if ($category_id) {
            $query .= " WHERE category_id=?i";
            $products_data = $this->db->getAll($query, 'products', $category_id);

        } else $products_data = $this->db->getAll($query, 'products');

        foreach ($products_data as $product) {
            $attributes = unserialize($product['attributes']);
            $data = $product;
            $data['attributes'] = $attributes;
            $result[] = $data;
        }
        return $result;
    }

    public function createQueue($url, $type, $shop = 1)
    {

        $category_queue = $this->db->getAll("SELECT id 
            FROM ?n
            WHERE `category_url`=?s AND `status`=0 AND `type`=?s AND `shop_id`=?s", 'queue', $url, $type, $shop);

        if (count($category_queue) > 0)
            return ['message' => 'Запрос уже создан, ожидайте выполнения', 'result' => false];


        $sql = "INSERT INTO queue SET date_add=NOW(),?u";
        if ($this->db->query($sql, [
            'type' => $type,
            'category_url' => $url,
            'shop_id' => $shop,
            'status' => 0
        ])
        )
            return ['message' => 'Задача поставлена в очередь. Ожидайте', 'result' => true];


        return ['message' => 'Произошла ошибка', 'result' => false];
    }

    public function saveCsvInDB($item_id, $filename)
    {
        $sql = "UPDATE queue SET csv_file=?s,date_csv_file=?s WHERE id=?i";
        return $this->db->query($sql, $filename, date('Y-m-d H:i:s'), $item_id);
    }

    public function parser_queue()
    {
        $queue = $this->db->getAll("SELECT * 
            FROM ?n
            WHERE status=0", 'queue');
        foreach ($queue as $item) {

            $this->change_status_queue($item['id'], 2);// сменяем статус на В процессе
            $shop_id = $item['shop_id'];
            $queue_id = $item['id'];
            $url = $item['category_url'];

            if ($this->newUrl($url, $shop_id)) {
                $category_id = $this->db->getOne("SELECT id 
                    FROM ?n
                    WHERE url=?s", 'category', $url);



                switch ($item['type']) {
                    case 'parser':
                        if ($result = $this->parserCategoriesWithProducts($category_id, false, $queue_id)) {
                            if ($result['result']) {
                                $this->change_status_queue($item['id'], 1);
                                $this->saveCSV($item['id'], $category_id);
                            } else
                                $this->error_queue($item['id'], $result['error']);
                        }
                        break;
                    case 'price':
                        if ($result = $this->parserCategoriesWithProducts($category_id, true, $queue_id)) {

                            if ($result['result']) {
                                $this->change_status_queue($queue_id, 1);
                                $this->saveCSV($queue_id, $category_id);
                            } else
                                $this->error_queue($queue_id, $result['errors']);
                        }
                        break;
                    case 'get_csv':
                        $this->saveCSV($queue_id, $category_id);
                        break;

                }
            }
        }
    }


    public function saveCSV($item_id=false, $category_id=false)
    {
        $_products = $this->getSubCategoriesFromDB($category_id);
        $products = $_products['products'];

        $delimeter = '~';
        $products_csv = '';
        $headers_array = ['Артикул', 'Название', 'Штрихкод', 'Цена', 'Бренд', 'Фасовка'];
        $header = implode($delimeter, $headers_array);//'Артикул'..'Название;Штрихкод;Цена;Бренд;Фасовка;';
        $headers_array_attr = [];

        foreach ($products as $product) {
            $attributes = $product['attributes'];
            if ($attributes) {
                if (is_array($attributes)) {
                    foreach ($attributes as $attr) {
                        $attr_explode = explode('=', $attr);
                        $name = $attr_explode[0];
                        $value = $attr_explode[1];
                        $headers_array_attr[$name] = $value;
                    }
                }
            }
        }

        $products_csv .= $header . implode($delimeter, $headers_array_attr) . "\r\n";
        foreach ($products as $product) {

            $attributes = $product['attributes'];
            $name = $product['name'];
            $name = str_replace('&nbsp;', '', $name);
            $name = str_replace('&quot;', '', $name);

            $new_data = [
                'sku' => $product['sku'],
                'name' => $name,
                'barcode' => $product['barcode'],
                'price' => $product['price'],
                'brand' => $product['brand'],
                'packaging' => $product['packaging']
            ];
            $temp_attr = [];
            if (is_array($attributes)) {
                foreach ($attributes as $attr) {
                    $attr_explode = explode('=', $attr);

                    $name = $attr_explode[0];
                    $value = $attr_explode[1];
                    $temp_attr[$name] = $value;

                }
            }


            foreach ($headers_array_attr as $name => $value) {

                if (isset($temp_attr[$name])) {
                    $new_data[$name] = $temp_attr[$name];
                } else {
                    $new_data[$name] = '';
                }
            }
            $products_csv .= implode($delimeter, $new_data) . ";\r\n";


        }


        $root = '/var/www/parser.finer.ua';

        $file = '/files/price' . date('YmdHis') . '.csv';
        $handle = fopen($root . $file, "w+");

        fwrite($handle, $products_csv);
        fclose($handle);

        if ($item_id) {
            if ($this->saveCsvInDB($item_id, $file)) {
                $this->change_status_queue($item_id, 1);
            }
        }else return $this->url_parser.$file;


        return true;
    }


    public function getTypesQueue()
    {
        return [
            'parser' => 'парсить ещё раз',
            'get_csv' => 'Получить csv',
            'price' => 'Обновить только цены'
        ];
    }

    public function error_queue($id, $error)
    {
        $sql = "UPDATE queue SET errors=?s WHERE id=?i";
        $this->db->query($sql, $error, $id);
    }

    /**
     * @return array
     */
    public function list_queue()
    {
        $result = [];

        $query = "SELECT * FROM ?n ORDER BY date_add DESC";
        $products_data = $this->db->getAll($query, 'queue');

        foreach ($products_data as $product) {
            $data = $product;
            $result[] = $data;
        }
        return $result;
    }


    public function sendMail($queue_id)
    {
        $admin_emails = $this->admin_emails;

        $queues = $this->db->getAll("SELECT * 
                    FROM ?n
                    WHERE id=?s", 'queue', $queue_id);

        foreach ($queues as $queue)
        {
            $linkcsv=$this->url_parser.$queue['csv_file'];
            $message='<p>Закончился парсинг категории '.$queue['category_url'].'<br/> Можно скачать: <a href="'.$linkcsv.'">'.$linkcsv.'</a> </p>';

            mail(implode(', ',$admin_emails), 'Парсер категории '.$queue['category_url'], $message);

            break;
        }

    }

    /**
     * @param $id
     * @param int $status
     */
    public function change_status_queue($id, $status = 1)
    {
        if ($status == 1) {
            $sql = "UPDATE queue SET status=?i, date_parser=NOW() WHERE id=?s";
            $this->db->query($sql, $status, $id);

            $this->sendMail($id);
        } else {
            $sql = "UPDATE queue SET status=?i WHERE id=?s";
            $this->db->query($sql, $status, $id);
        }
    }


    /**
     *
     * блок по обновлению цен по крону
     *
     */


    /**
     *
     */
    public function parser_prices()
    {
        $config_parser_prices = $this->db->getRow("SELECT * 
                    FROM ?n
                    WHERE code=?s", 'config', 'parser_price');

        if ($config_parser_prices['value']==1)
        {
            // идём обновлять цены
            $result=$this->process_parser_price();

            $config_admin_emails = $this->db->getRow("SELECT * 
                    FROM ?n
                    WHERE code=?s", 'config', 'email_admin');
            $emails=$config_admin_emails['value'];

            $this->sendNotificationPrices($emails,$result);
            // отправил уведомление админам
        }
        //parser_price

    }

    public function updateissetProduct($url,$status)
    {
        $sql = "UPDATE category SET isset_products=?i, date_update=NOW() WHERE url=?s";
        if ($this->db->query($sql, $status, $url)) return true;

        return false;

    }

    public function updatePriceProduct($url,$price)
    {
        $sql = "UPDATE products SET price=?s, date_update=NOW() WHERE url=?s";
        if ($this->db->query($sql, $price, $url))
            return true;

        return false;
    }


    public function get_content_category_for_price($url)
    {
        sleep(1);
        $flag_update = true;//false;
        $base_url = $this->base_url;

        $_response_arr = $this->curl_get_contents($url, $base_url, 5, 1);
        $page = $_response_arr['html'];
        $page_code = $_response_arr['http_code'];
        $res_arr['error_page'] = $_response_arr['errors'];

        if (!empty($page) && $page_code == 200) {

/*
            // ищем субкатегории
            $buffer = [];
            $regexp = "/<div id=\"subcategories\">(.*)<\/div>/Us";
            $regexp = "/<a class=\"subcategory-name\" href=\"(.*)\">(.*)<\/a>/Us";

            preg_match_all($regexp, $page, $buffer);

            $sub_categories = [];

            if (isset($buffer[1])) {
                foreach ($buffer[1] as $key => $_sub_category) {
                    if (isset($buffer[2])) {

                        $sub_categories[] = 1;//$name_sub;//$sub_category_insert;
                    }
                }
            }

            if (count($sub_categories) > 0) {

                //$this->updateissetProduct($url,0);
                return false;
            } else {*/
                // если есть товары парсим цены товаров
                //$this->updateissetProduct($url,1);

                    //$price_products=
                        $this->getProductsFromHtmlPrice($page);

                    // теперь необходимо пройтись по пагинации
                    //$price_products =
                        $this->getProductFromPaginationPrice($page, []);//$price_products);

           // }


            return true;
        }

            return false;



        //return $res_arr;
    }



    /**
     * @param $page
     * @return bool|mixed
     */
    public function getProductsFromHtmlPrice($page)
    {
        $buffer = [];
        $regexp = "/<ul id=\"product_list\" class=\"product_list grid row\">(.*)<\/ul>/Us";
        preg_match($regexp, $page, $buffer);
        $prices=[];
        if (isset($buffer[1])) {
            $page_list_products = $buffer[1];



            $regexp = "/<li class=\"ajax_block_product (.*)\">(.*)<\/li>/Us";
            $buffer = [];
            preg_match_all($regexp, $page_list_products, $buffer);
            if (isset($buffer[2]))
            {

                /*echo "<pre>";
                print_r($buffer[2]);
                echo "</pre>";*/
                foreach ($buffer[2] as $product_html)
                {


                    $regexp = "/<a class=\"product-name\" href=\"(.*)\" title=\"(.*)\">(.*)<\/a>/Us";
                    $buffer_product_url = [];
                    preg_match($regexp, $product_html, $buffer_product_url);
                   /* echo "<pre>hhhh";
                    print_r($buffer_product_url);
                    echo "</pre>";*/
                    if (isset($buffer_product_url[1])) {
                        $url_product = $buffer_product_url[1];

                        $price_product=0;

                        //.content_price .product-price
                        //<span class="price product-price">
                        $regexp = "/<span class=\"price product-price\">(.*)<\/span>/Us";
                        $buffer_product_price = [];
                        preg_match($regexp, $product_html, $buffer_product_price);
                        if (isset($buffer_product_price[1])) {
                            $price_product = $buffer_product_price[1];
                        }

                        //$parse_url =
                       // $price_product=str_replace('грн','',$price_product);

                        $price = strip_tags($price_product);
                        $price = trim(str_replace('грн', '', $price));
                        $price = trim(str_replace('грн.', '', $price));
                        $price_ex = explode(' ', $price);
                        //if (isset($price_ex[0]))
                        $price = trim(str_replace(',', '.', $price_ex[0]));
                        $price = (float)$price;


                        $prices[$url_product]=$price;//trim($price_product);

                        // обновляем цены в базе
                        //echo 'product '.$url_product." ".trim($price_product)."\r\n";
                        $this->updatePriceProduct($url_product,trim($price));
                    }
                    //echo "update price\r\n";
                }
            }



        }

        return $prices;
    }

    /**
     * @param $page
     * @param $href_products
     * @return array
     */
    public function getProductFromPaginationPrice($page, $href_products)
    {
        $buffer = [];
        $regexp = "/<li id=\"pagination_next\" class=\"pagination_next\">(.*)<\/li>/Us";

        preg_match($regexp, $page, $buffer);
        if (isset($buffer[1])) {
            $page_list_products = $buffer[1];
            $regexp = "/<a href=\"(.*)\"\s*>(.*)<\/a>/Us";
            $buffer = [];
            preg_match($regexp, $page_list_products, $buffer);
            if (isset($buffer[1])) {
                $url = str_replace('rel="next', '', $buffer[1]);
                $url = trim($this->base_url_slesh . str_replace('"', '', $url));

                $_response_arr = $this->curl_get_contents($url, $this->base_url, 5, 1);
                $page = $_response_arr['html'];
                $page_code = $_response_arr['http_code'];
                if (!empty($page) && $page_code == 200) {
                    $href_products_temp = $this->getProductsFromHtmlPrice($page);
                    $href_products = array_merge($href_products, $href_products_temp);
                    // ищем рекурсивно по ссылке Следующая страница
                    return $this->getProductFromPaginationPrice($page, $href_products);
                }
            }

        }
        return $href_products;
    }




    /**
     *
     */
    public function process_parser_price()
    {
        //$this->get_content_category_for_price('http://fozzyshop.com.ua/300284-sredstva-po-ukhodu-za-rukami');
        //die();
        $categories_data = $this->db->getAll("SELECT id,url,name,shop_id,parent,parent_id FROM ?n WHERE isset_products='1'", 'category');
        //$categories_data = $this->getListCategories();
        $row=0;
        foreach ($categories_data as $category) {
            $url = $category['url'];
            //echo $url . "<br/>\r\n";
            $this->get_content_category_for_price($url);
            $row++;
            echo $row.' '.$url."\r\n";

            @ob_flush();
            flush();
            //sleep(1);
        }
        $file=$this->saveCSV();

        return $file;

    }


    public function sendNotificationPrices($emails,$file)
    {
        $emails_array=explode(';',$emails);
        $message='Спарсились цены.  Можно скачать: '.$file.'';
            mail('portnovvit@gmail.com, s@finer.ua', 'Парсер цен', $message);
        /*foreach ($emails_array as $email)
        {



            mail($email, 'Парсер цен', $message);

            break;
        }*/
    }

}