/**
 * Created by portn on 26.12.2017.
 */
jQuery(document).ready(function() {
    jQuery('#dataTable').DataTable({
        "order": [[ 1, "desc" ]]
    });
    jQuery('.action_button').on('click',function(){
        url=jQuery(this).data('url');
        type=jQuery(this).data('type');
        shop=jQuery(this).data('shop');
        path='/record_queue.php?shop='+shop+'&type='+type+'&url='+encodeURI(url);
        jQuery.ajax({
            type: "GET",
            url: path,
            //data: form,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false

        }).done(function (msg) {
            //if (!msg.result)
                alert(msg.message);

            //else

            /*if (msg.html!='') {
                $('#editAvatar').modal('show');
                $('#editAvatar .boxeditavatar').html(msg.html);
            }*/

            // something...

        }).fail(function (msg) {
            alert('Возникла ошибка. Попробуйте позже');
        });
        return false;
    });
} );