<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 27.12.2017
 * Time: 13:03
 */

setlocale(LC_ALL, 'ru_RU.UTF-8');
header('Content-Type: text/html; charset=utf-8', true);
require_once('lib/Parser.php');


ob_implicit_flush(1);

echo str_pad('', 1024);
@ob_flush();
flush();
$parser=new Parser();
//$url,$type
if (isset($_GET['url'])&&isset($_GET['type']))
    echo json_encode($parser->createQueue($_GET['url'],$_GET['type'],$_GET['shop']));
else echo json_encode(['result'=>false,'message'=>'Error']);