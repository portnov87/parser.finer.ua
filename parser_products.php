<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 04.12.2017
 * Time: 17:17
 */

setlocale(LC_ALL, 'ru_RU.UTF-8');
header('Content-Type: text/html; charset=utf-8', true);
require_once('lib/Parser.php');


ob_implicit_flush(1);

echo str_pad('', 1024);
@ob_flush();
flush();
$parser=new Parser();

$categories=$parser->getListCategories();

foreach ($categories as $category)
{
    $result=$parser->getProducts($category['url'],false);
}