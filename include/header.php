<?php

header("Cache-Control: no-store, no-cache, must-revalidate");
header("Expires: " . date("r"));
ini_set("memory_limit","256M");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge">-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php if (isset($meta['title'])&&($meta['title']!='')) echo $meta['title']?></title>
    <!-- Bootstrap -->
    <link  type="text/css" media="all" href="/css/bootstrap/bootstrap.css" rel="stylesheet">



    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-2.2.3/dt-1.10.12/fh-3.1.2/r-2.1.0/datatables.min.css"/>

    <link rel="stylesheet" href="/css/responsive.dataTables.css" />
    <link rel="stylesheet" href="/css/styles.css" />
<style>
    .listcommand li{display:inline;margin-right:10px;}
</style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12" style="clear:both; text-align:center;padding:15px 0px;">
            <ul class="listcommand">
                <li><a href="/list_queue.php">Очереди</a></li>
                <li><a href="/">Новый запрос</a></li>
            </ul>
        </div>
    </div>
</div>



