<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 03.12.2017
 * Time: 15:58
 */
$root=$_SERVER['DOCUMENT_ROOT'];
define('PATH_INCLUDE',$root.'/include/');
setlocale(LC_ALL, 'ru_RU.UTF-8');
header('Content-Type: text/html; charset=utf-8', true);
require_once('lib/Parser.php');

//$category_url='http://fozzyshop.com.ua/300008-tovary-dlya-detej';
$parser=new Parser();
$list_parser=$parser->list_queue();
$meta=array('title'=>'Finer парсер','desciption'=>'');
include(PATH_INCLUDE.'header.php');
$shop=false;
?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <h1>Очереди обработок</h1>
                <table id="dataTable" class="display" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Урл</th>
                            <th>Дата</th>
                            <th>Тип</th>
                            <th>Статус</th>
                            <th>CSV файл</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($list_parser as $item)
                    {
                        ?>
                        <tr>
                            <td><?php echo $item['category_url'];?></td>
                            <td><?php echo $item['date_add'];?></td>
                            <td><?php
                                switch ($item['type']){
                                    case 'price':
                                        echo 'Обновление цен';
                                        break;
                                    case 'parser':
                                        echo 'Парсим урлы';
                                        break;
                                    case 'get_csv':
                                        echo 'Запрос на формирование цсв';
                                        break;
                                }
                                ?></td>
                            <td><?php switch ($item['status'])
                                {
                                    case '0':
                                        echo 'Ожидание обработки';
                                        break;
                                    case '1':
                                        echo 'Готово';
                                        break;
                                    case '2':
                                        echo 'В процессе';
                                        break;
                                }
                                ?></td>
                            <td><?php
                                if ($item['csv_file']!='')
                                {
                                echo '<a href="'.$item['csv_file'].'">Скачать файл</a>';
                                }

                                ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

<?php
$scripts=array('/js/script.js'); include(PATH_INCLUDE.'footer.php');?>


